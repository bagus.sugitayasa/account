/**
 * 
 */
package id.training.telkomsigma.marketplace.account.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version Id: 
 */
@Configuration
@EnableAuthorizationServer
public class KonfigurasiAuthserver extends AuthorizationServerConfigurerAdapter {
	
	@Autowired
	@Qualifier("authenticationManagerBean")
	private AuthenticationManager authenticationManager;
	
	@Autowired
	@Qualifier("userDetailsServiceBean")
	private UserDetailsService userDetailsService;
	
	@Autowired private DataSource dataSource;
	
	private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	
	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		clients.jdbc(dataSource).passwordEncoder(passwordEncoder);
//        .withClient("authcodeclient")
//            .secret("authcode321")
//            .resourceIds("marketplace")
//            .redirectUris("http://example.com")
//            .authorizedGrantTypes("authorization_code","refresh_token")
//            .authorities("APLIKASI_CLIENT_OAUTH2")
//            .scopes("demo")
//            .accessTokenValiditySeconds(300)
//            .refreshTokenValiditySeconds(3000)
//        .and().withClient("implicitclient")
//            .resourceIds("marketplace")
//            .redirectUris("http://example.com")
//            .authorizedGrantTypes("implicit")
//            .authorities("APLIKASI_CLIENT_OAUTH2")
//            .scopes("demo")
//        .and().withClient("userpassword")
//            .resourceIds("marketplace")
//            .redirectUris("http://example.com")
//            .authorizedGrantTypes("password")
//            .authorities("APLIKASI_CLIENT_OAUTH2")
//            .scopes("demo")
//            .accessTokenValiditySeconds(1800)
//            .refreshTokenValiditySeconds(18000)
//            .autoApprove(true)
//        .and().withClient("clientapp")
//            .resourceIds("marketplace")
//            .redirectUris("http://example.com")
//            .authorizedGrantTypes("client_credentials")
//            .authorities("APLIKASI_CLIENT_OAUTH2")
//            .scopes("demo")
//            .autoApprove(true)
//        ;
	}
	
	@Override
	public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
		security.checkTokenAccess("hasAuthority('APLIKASI_CLIENT_OAUTH2')").tokenKeyAccess("permitAll()").passwordEncoder(passwordEncoder);
	}
	
	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		endpoints.authenticationManager(authenticationManager).userDetailsService(userDetailsService);
	}
}
